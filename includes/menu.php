<?php
$islogged = false;
if(session_id() == '') {
    session_start();
}
if (isset($_SESSION['user'])) {
	$islogged = true;
}
?>
<div id="menu">
	<div id="menu_pages">
		<a href="index.php">Partitions publiques</a>
		<?php
			if ($islogged) {
				echo ' | <a href="index.php?type=my">Mes partitions</a>' .
					' | <a href="index.php?type=fav">Mes partitions favorites</a>' .
					' | <a href="edition_usager.php?action=edit">Mon profil</a>';
			}
		?>	
	</div>
	<div id="menu_logoff">
		<?php
			if ($islogged) {
				echo $_SESSION['user'] . ' <a href="traitement_login.php?action=logoff">[Déconnexion]</a>';
			}
			else {
				echo '<a href="login.php">[Connexion]</a>';
			}
		?>
	</div>
</div>
