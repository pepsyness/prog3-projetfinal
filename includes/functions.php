<?php

/*
function vd() {
	// Afficher tous les arrays et stopper
	echo 'POST:<br /><br />';
	var_dump($_POST);
	echo 'GET:<br /><br />';
	var_dump($_GET);
	echo 'SESSION:<br /><br />';
	var_dump($_SESSION);
	die();
}
*/

function do_orderby($what, $contexte) {
	// Contexte: 0 = défaut, 1 = my, 2 = fav
	if ($contexte == 0 || $contexte == 2) {
		switch ($what) {
			case "artiste":
				return ' ORDER BY Artiste, Titre';
				break;
			case "genre":
				return ' ORDER BY Genre, Titre';
				break;
			case "utilisateur":
				return ' ORDER BY Nom, Titre';
				break;
			default:
				return ' ORDER BY Titre';
				break;
		}
	}
	else if ($contexte == 1) {
		switch ($what) {
			case "artiste":
				return ' ORDER BY Artiste, Titre';
				break;
			case "genre":
				return ' ORDER BY Genre, Titre';
				break;
			case "acces":
				return ' ORDER BY IsPublic, Titre';
				break;
			default:
				return ' ORDER BY Titre';
				break;
		}
	}
	else {
		return ' ORDER BY Titre';
	}
}

?>