<?php
include('includes/constantes2.php');
include('includes/functions.php');
if(session_id() == '') {
    session_start();
}
$errMsg = '';
$partChoice = '';
$lastAction = '';
$fichierActuel = '';

if (isset($_GET['partChoice']) && $_GET['partChoice'] != null)
	$partChoice  = $_GET['partChoice'];
else if (isset($_POST['partChoice']) && $_POST['partChoice'] != null)
	$partChoice = $_POST['partChoice'];
if (isset($_GET['fichierActuel']) && $_GET['fichierActuel'] != null)
	$fichierActuel = $_GET['fichierActuel'];
else if (isset($_POST['fichierActuel']) && $_POST['fichierActuel'] != null)
	$fichierActuel = $_POST['fichierActuel'];
if (isset($_GET['lastAction']) && $_GET['lastAction'] != null)
	$lastAction = $_GET['lastAction'];
else if (isset($_POST['lastAction']) && $_POST['lastAction'] != null)
	$lastAction = $_POST['lastAction'];
?>
<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title>Envoi d'un fichier XML</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>
		<?php
		if (isset($_POST["submit"])) {
			$filename = basename($_FILES["fileToUpload"]["name"]);
			if ($filename != "") {
				$filesize = $_FILES["fileToUpload"]["size"];
				$target_file = "partitions/" . $filename;
				$filetype = pathinfo($target_file, PATHINFO_EXTENSION);
				$uploadOk = 1;

				if (file_exists($target_file)) {
					$errMsg = "Désolé, le fichier existe déjà.";
					$uploadOk = 0;
				}
				
				if ($filesize > 500000) {
					$errMsg =  "Désolé, votre fichier est trop volumineux.";
					$uploadOk = 0;
				}
				
				if($filetype != "xml") {
					$errMsg =  "Désolé, seuls les fichiers XML sont acceptés.";
					$uploadOk = 0;
				}

				if ($uploadOk == 1) {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						if (strcmp($lastAction, "Ajouter") != 0) {
							$deleteFile = 'partitions/' . $fichierActuel;
							unlink($deleteFile); // Effacer l'ancien fichier.
						}
						else
							$lastAction = "Modifier";
						$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
						mysql_select_db(DB_NAME, $db);
						$requete = 'UPDATE projet_partitions SET FichierXml = "' . $filename . '" WHERE UsagerID = ' . $_SESSION['userid'] .
										' AND ID = ' . $partChoice;
						$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
						$rowreqA = mysql_fetch_assoc($result);
						Header("Location: edition_part.php?action=$lastAction&part_choice=$partChoice&test=$fichierActuel");
					}
					else {
						$errMsg =  "Désolé, une erreur s'est produite lors de l'envoi.";
					}
				}
				
				echo "<br /><br />";
			}
		}
		?>
		<div id="content">
			<span class="title">Envoyer un fichier de partition XML</span><br /><br />
			<?php
				if ($fichierActuel != null) {
					echo 'Fichier XML actuel: ' . $fichierActuel . '<br /><br />';
				}
				echo '<span id="errortxt">' . $errMsg . '</span><br /><br />';
			?>
			<form action="upload.php" method="post" enctype="multipart/form-data">
				<input type="hidden" name="partChoice" value="<?php echo $partChoice; ?>" />
				<input type="hidden" name="lastAction" value="<?php echo $lastAction; ?>" />
				<input type="hidden" name="fichierActuel" value="<?php echo $fichierActuel; ?>" />
				Selectionnez un fichier:<br /><br />
				<input type="file" name="fileToUpload" id="fileToUpload">
				<br /><br />
				<input type="submit" name="submit" value="Envoyer">
				<?php
					echo '&nbsp;&nbsp;<input type="button" name="annuler" value="Annuler" onclick="goToParts(' . $partChoice . ', \'' . $lastAction . '\');" />';
				?>
			</form>
			<br /><br />
		</div>
		<?php include("includes/footer.php"); ?>
	</body>
</html>
