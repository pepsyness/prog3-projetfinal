<?php
include('includes/constantes2.php');
include('includes/functions.php');
if(session_id() == '') {
    session_start();
}
$type = "";
$page_titre = "Toutes les partitions publiques";
$logged = false;
$item_count = 0;
$orderby = ""; // 0 = Titre (défaut), 1 = Artiste, 2 = Genre, 3 = Utilisateur

if (isset($_GET["type"])) {
	$type = $_GET["type"];

	switch ($type) {
		case "my":
			$page_titre = "Mes partitions";
			break;

		case "fav":
			$page_titre = "Mes partitions favorites";
			break;
		
		default:
			$type = "";
	}
}

if (isset($_GET['orderby'])) {
	$orderby = $_GET['orderby'];
}

if (isset($_SESSION["user"])) {
	$logged = true;
	// Récupérer les favoris
	unset($_SESSION['fav']);
	$_SESSION['has_fav'] = false;
	$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
	mysql_select_db(DB_NAME, $db);
	$requete = 'SELECT PartitionID FROM projet_favoris ' .
		'INNER JOIN projet_usagers ' .
		'ON projet_favoris.UsagerID = projet_usagers.ID ' .
		'WHERE projet_usagers.ID = ' . $_SESSION['userid'];
	$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
	if (mysql_num_rows($result) > 0) {
		$_SESSION['has_fav'] = true;
		$x = 0;
		$tmpstr = "";
		while ($row = mysql_fetch_assoc($result)) {
			$tmpstr .= ' ' . $row['PartitionID'];
			$x++;
		}
		$_SESSION['fav_total'] = $x;
		$_SESSION['fav'] = explode(' ', $tmpstr);
	}
	else
		$_SESSION['has_fav'] = false;
}
?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title><?php echo $page_titre; ?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<?php
		if (isset($_GET['changeFile']) && $_GET['changeFile'] != null && isset($_GET['partid']) && $_GET['partid'] != null)
			// Modifier FichierXml de la partition sans fichier pour "Aucun"
			echo '<body onload="changerFichier(' . $_GET['partid'] . ');">';
		else
			echo '<body>';
	?>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>

		<div id="content">
			<span class="title"><?php echo $page_titre; ?></span>
			<?php
				if ($logged && $type == null)
					echo '<br /><span class="subtitle">(Excluant les vôtres)</span>';
			?>
			<br /><br />
			<div class="content_left">
			<?php
				// *********** DÉBUT TYPE DÉFAUT ***********
				if ($type == '') {
					$item_count = 0;
					if ($logged)
						echo '<table class="table_list_ext">';
					else
						echo '<table class="table_list">';
					?>
					<tr>
						<?php
						if ($logged)
							echo '<td></td>';
						?>
						<td><a style="color:#3c95f7;" href="index.php?orderby=titre">Titre</a></td>
						<td><a style="color:#3c95f7;" href="index.php?orderby=artiste">Artiste</a></td>
						<td><a style="color:#3c95f7;" href="index.php?orderby=genre">Genre</a></td>
						<td><a style="color:#3c95f7;" href="index.php?orderby=utilisateur">Utilisateur</a></td>
					</tr>
					<?php 
					$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
					mysql_select_db(DB_NAME, $db);
					// Lister les partitions
					$requete = 'SELECT projet_partitions.ID, Titre, Artiste, Genre, Nom FROM projet_partitions ';
					$requete .= 'INNER JOIN projet_usagers ON projet_usagers.ID = projet_partitions.UsagerID ';
					if ($logged)
							$requete .= 'WHERE projet_usagers.ID <> ' . $_SESSION['userid'] . ' AND IsPublic=TRUE';
					else
						$requete .= 'WHERE IsPublic=TRUE';
					$requete .= do_orderby($orderby,0);
					$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
					$id_part = 0;
					$item_count = 0;
					if ($logged) {
						// Scanner les partitions qui sont favorites
						while ($row = mysql_fetch_assoc($result)) {
							$id_part = $row['ID'];
							$fav_found = false;
							echo '<tr>';
							echo '<td><input type="hidden" name="item' . $item_count . '" value="' . $id_part . '" /><input type="radio" name="part_choice" /></td>';
							// Si l'usager a des favoris, les mettre en gras
							if ($_SESSION['has_fav']) {
								$x = $_SESSION['fav_total'];
								while ($x > 0) {
									if (strcmp($_SESSION['fav'][$x], $id_part) == 0) {
										$titre = '<b>' . $row['Titre'] . '</b>';
										$artiste = '<b>' . $row['Artiste'] . '</b>';
										$genre = '<b>' . $row['Genre'] . '</b>';
										$nom = '<b>' . $row['Nom'] . '</b>';
										$fav_found = true;
										break;
									}
									$x--;
								}	
							}
							if (!$fav_found) {
								$titre = $row['Titre'];
								$artiste = $row['Artiste'];
								$genre = $row['Genre'];
								$nom = $row['Nom'];
							}
							echo '<td><span class="fake_link" onclick="showpartinfo(' . $id_part . ')">' . $titre . '</span></td>';
							echo '<td>' . $artiste . '</td>';
							echo '<td>' . $genre . '</td>';
							echo '<td>' . $nom . '</td>';
							echo '</tr>';
							$item_count++;
						}
					}
					else {
						while ($row = mysql_fetch_assoc($result)) {
							$id_part = $row['ID'];
							echo '<tr>';
							echo '<td><span class="fake_link" onclick="showpartinfo(' . $id_part . ')">' . $row['Titre'] . '</span></td>';
							echo '<td>' . $row['Artiste'] . '</td>';
							echo '<td>' . $row['Genre'] . '</td>';
							echo '<td>' . $row['Nom'] . '</td>';
							echo '</tr>';
						}
					}
				}
				// *********** FIN TYPE DÉFAUT ***********
				// *********** DÉBUT TYPE FAV ***********
				else if (strcmp($type, "fav") == 0 && $logged) {
					$item_count = 0;
					?>
					<table class="table_list_ext">
						<tr>
							<td></td>
							<td><a style="color:#3c95f7;" href="index.php?type=fav&orderby=titre">Titre</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=fav&orderby=artiste">Artiste</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=fav&orderby=genre">Genre</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=fav&orderby=utilisateur">Utilisateur</a></td>
						</tr>
						<?php
						$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
						mysql_select_db(DB_NAME, $db);
						// Lister les favoris
						$requete = 'SELECT projet_partitions.ID, Titre, Artiste, Genre, Nom FROM projet_partitions ' .
										'INNER JOIN projet_usagers ' .
										'ON projet_usagers.ID = projet_partitions.UsagerID ' .
										'WHERE projet_partitions.ID IN ' .
												'(SELECT projet_favoris.PartitionID FROM projet_favoris ' .
												'WHERE projet_favoris.UsagerID = ' . $_SESSION['userid'] . ')';
						$requete .= do_orderby($orderby,2);
						$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
						while ($row = mysql_fetch_assoc($result)) {
							$id_part = $row['ID'];
							echo '<tr>';
							echo '<td><input type="hidden" name="item' . $item_count . '" value="' . $id_part . '" /><input type="radio" name="part_choice" /></td>';
							echo '<td><span class="fake_link" onclick="showpartinfo('  .$id_part . ')">' . $row['Titre'] . '</span></td>';
							echo '<td>' . $row['Artiste'] . '</td>';
							echo '<td>' . $row['Genre'] . '</td>';
							echo '<td>' . $row['Nom'] . '</td>';
							echo '</tr>';
							$item_count++;
						}
				}
				else if (strcmp($type, "my") == 0 && $logged) {
					?>
					<table class="table_list_ext">
						<tr>
							<td></td>
							<td><a style="color:#3c95f7;" href="index.php?type=my&orderby=titre">Titre</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=my&orderby=artiste">Artiste</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=my&orderby=genre">Genre</a></td>
							<td><a style="color:#3c95f7;" href="index.php?type=my&orderby=acces">Accès</a></td>
						</tr>
						<?php
						$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
						mysql_select_db(DB_NAME, $db);
						$requete = 'SELECT ID, Titre, Artiste, Genre, IsPublic FROM projet_partitions ' .
										'WHERE UsagerID=' . $_SESSION['userid'];
						$requete .= do_orderby($orderby,1);
						$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
						while ($row = mysql_fetch_assoc($result)) {
							$id_part = $row['ID'];
							echo '<tr>';
							echo '<td><input type="radio" name="part_choice" value="' . $id_part . '" /></td>';
							echo '<td><span class="fake_link" onclick="showpartinfo('  .$id_part . ')">' . $row['Titre'] . '</span></td>';
							echo '<td>' . $row['Artiste'] . '</td>';
							echo '<td>' . $row['Genre'] . '</td>';
							if ($row['IsPublic'] == 1)
								echo '<td>Publique</td>';
							else
								echo '<td>Privé</td>';
							echo '</tr>';
							$item_count++;
						}
				}
				else
					Header("Location: index.php");
				echo '</table>';
				if ($logged) {
					if ($type == null) {
						echo '<br />';
						echo '<span class="subtitle" id="msgGlobal">Vos partitions favorites apparaissent en <b>gras</b></span>';
					echo '<br /><br />';
					echo '<input type="button" class="btn" name="add_fav" value="Ajouter à mes favoris" onclick="addFavForUser(' . $_SESSION['userid'] . ')">';
					}
					else if (strcmp($type, "fav") == 0) {
						echo '<br />';
						echo '<input type="button" class="btn" name="del_fav" value="Supprimer de mes favoris" onclick="delFavForUser(' . $_SESSION['userid'] . ')">';
					}
					else if (strcmp($type, "my") == 0) {
						echo '<br />';
						echo '<input type="submit" class="btn" name="action" value="Ajouter" onclick="validChoice(this);">';
						echo '  <input type="submit" class="btn" name="action" value="Modifier" onclick="validChoice(this);">';
						echo '  <input type="button" class="btn" name="action" value="Supprimer" onclick="delPartForUser(' . $_SESSION['userid'] . ');" />';
					}
				}
				?>
				<br /><br />
				<span id="errortxt"> </span>
			</div>
			<div class="content_right">
				<span class="span_item_info_titre" id="part_titre"> </span>
				<br /><br />
				<table class="table_item_info" id="tableshowinfo">
					<tr>
						<td>Artiste:</td><td></td>
					</tr>
					<tr>
						<td>Genre:</td><td></td>
					</tr>
					<tr>
						<td>Publique:</td><td>
					</tr>
					<tr>
						<td>Utilisateur:</td><td></td>
					</tr>
					<tr>
						<td>Ajoutée le:</td><td></td>
					</tr>
					<tr>
						<td>Description:</td><td></td>
					</tr>
					<tr>
						<td>Fichier:</td><td></td>
					</tr>
				</table>
				<input type="hidden" name="loggedOrNot" id="verif_session" value="<?php
					if ($logged)
						echo "yes";
					else
						echo "no";
					echo '">'
					?>
			</div>
			<br /><br />
		</div>
		<?php include("includes/footer.php"); ?>
	</body>
</html>
