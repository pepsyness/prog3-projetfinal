﻿

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title>Identification</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>
	
		<div id="content">
			<span class="title">Identification</span>
			<br /><br />

			<form method="post" action="traitement_login.php" onsubmit="return verif_login();">
				<table class="edition">
					<tr>
						<td>Nom d'utilisateur :</td>
						<td><input type="text" name="txtNom" id="txtNom" value=""></td>
					</tr>
					<tr class="trerror">
						<td colspan="2" style="text-align:center"><span id="errloginname"> </span>
					</tr>
					<tr>
						<td>Mot de passe :</td>
						<td><input type="password" name="txtMotPasse" id="txtMotPasse" value=""></td>
					</tr>
					<tr class="trerror">
						<td colspan="2" style="text-align:center"><span id="errpass"> </span></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" value="Connexion"></td>
					</tr>
				</table>
				
				<br />
				<?php
				if (isset($_GET['loginerr'])) {
					echo '<span id="errortxt">Échec de l\'identification.</span>';
				}
				?>
				<br /><br />
				<a href="edition_usager.php?action=add">Cliquer ici pour créer un nouvel utilisateur</a>
				<br /><br />
				<input type="hidden" name="action" value="login" />
			</form>
		</div>
		
		<?php include("includes/footer.php"); ?>
	</body>
</html>