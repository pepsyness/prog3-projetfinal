<?php
if(session_id() == '') {
    session_start();
}
$action = "";
$page_titre = "";
$bouton_texte = "";
$actionmode = 0; // 1 = Ajouter, 2 = Éditer
$logged = false;

if (isset($_GET["action"])) {
	$action = $_GET["action"];
	
	switch ($action) {
		case "add":
			$page_titre = "Ajout d'un utilisateur";
			$bouton_texte = "Ajouter";
			$actionmode = 1;
			break;
		
		case "edit":
			if (isset($_SESSION['userid'])) {
				$logged = true;
				$page_titre = "Mon profil utilisateur";
				$bouton_texte = "Modifier";
				$actionmode = 2;
			}
			else
				Header("Location: index.php");
			break;
		default:
			Header("Location: index.php");
			break;
	}
}
else {
	Header("Location: index.php");
}
?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title><?php echo $page_titre ?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>

		<div id="content">
			<span class="title"><?php echo $page_titre ?></span>
			<br /><br />
		
			<form method="post" action="traitement_usager.php" onsubmit="return verif_edit_user(<?php echo $actionmode; ?>);">
				<table class="edition">
					<tr>
						<td>Votre nom : </td>
						<td><input class="edition" type="text" id="txtNom" name="txtNom" value="<?php
								if ($actionmode == 2)
									echo $_SESSION['user'];
							?>" /></td>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errnom"> </span></td>
					</tr>
					<tr>
						<td>Nom d'utilisateur : </td>
						<td><input class="edition" type="text" id="txtNomUtilisateur" name="txtNomUtilisateur" value="<?php
								if ($actionmode == 2 && $logged)
									echo $_SESSION['username'];
							?>" /></td>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errloginname"> </span></td>
					</tr>
					<?php
						if ($actionmode == 2 && $logged) {
							echo '<tr>';
							echo '<td colspan="3"><span class="passwdmsg">Emplissez les cases suivantes seulement en cas de changement du mot de passe.</span></td>';
							echo '</tr>';
						}
					?>
					<tr>
						<td>Mot de passe : </td>
						<td><input class="edition" type="password" id="txtMotDePasse" name="txtMotDePasse" value="" /></td>
						<?php
							if ($actionmode == 1)
								echo '<input type="hidden" name="action" id="action" value="1" />';
							else if ($actionmode == 2)
								echo '<input type="hidden" name="action" id="action" value="2" />';
						?>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errpass"> </span></td>
					</tr>
					<tr>
						<td>Confirmation : </td>
						<td><input class="edition" type="password" id="txtConfirmation" name="txtConfirmation" value="" /></td>
					</tr>
				</table>

				<br />
				<input type="submit" class="btn" value="<?php echo $bouton_texte; ?>" />
				<input type="button" class="btn" value="Annuler" onclick="location.href='index.php';" />
				<br /><br />
			</form>
		</div>
		
		<?php include("includes/footer.php"); ?>
	</body>
</html>
