function showpartinfo(id) {
	var xhr = GetXHR();

	xhr.onreadystatechange = function() {
		if (XHRStateIsReady(xhr)) {
			showpartinforeply(xhr.responseXML);
		}
	}
	xhr.open("GET", "data/data_select.php?id=" + id, true);
	xhr.send(null);
}

function GetXHR() {
	var xhr = null;
	
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else {
		alert("Votre navigateur n'est pas compatible avec AJAX...");
	}
	
	return xhr;
}

function showpartinforeply(xmlDoc) {
	var lapartition
	var tablehtml
	var old_td
	var new_td;
	var new_span_text;
	var old_span_text;
	var hidden_val; // Pour vérifier si l'usager est identifié
	lapartition = xmlDoc.getElementsByTagName("partition")[0];
	spantitre = document.getElementById("part_titre");
	tablehtml = document.getElementById("tableshowinfo");
	old_span_text = spantitre.firstChild;
	new_span_text = document.createElement("span");
	new_span_text.appendChild(document.createTextNode(xmlDoc.getElementsByTagName("titre")[0].firstChild.nodeValue));
	spantitre.replaceChild(new_span_text, old_span_text);
	// Artiste
	old_td = tablehtml.getElementsByTagName("tr")[0].getElementsByTagName("td")[1];
	new_td = createtdnode(lapartition.getElementsByTagName("artiste")[0].firstChild.nodeValue);
	old_td.parentNode.replaceChild(new_td, old_td);
	// Genre
	old_td = tablehtml.getElementsByTagName("tr")[1].getElementsByTagName("td")[1];
	new_td = createtdnode(lapartition.getElementsByTagName("genre")[0].firstChild.nodeValue);
	old_td.parentNode.replaceChild(new_td, old_td);
	// Publique
	old_td = tablehtml.getElementsByTagName("tr")[2].getElementsByTagName("td")[1];
	if (lapartition.getElementsByTagName("publique")[0].firstChild.nodeValue > 0)
		new_td = createtdnode("Oui");
	else
		new_td = createtdnode("Non");
	old_td.parentNode.replaceChild(new_td, old_td);
	// Poster
	old_td = tablehtml.getElementsByTagName("tr")[3].getElementsByTagName("td")[1];
	new_td = createtdnode(lapartition.getElementsByTagName("nom_poster")[0].firstChild.nodeValue);
	old_td.parentNode.replaceChild(new_td, old_td);
	// Date d'ajout
	old_td = tablehtml.getElementsByTagName("tr")[4].getElementsByTagName("td")[1];
	new_td = createtdnode(lapartition.getElementsByTagName("date_ajout")[0].firstChild.nodeValue);
	old_td.parentNode.replaceChild(new_td, old_td);
	// Description
	old_td = tablehtml.getElementsByTagName("tr")[5].getElementsByTagName("td")[1];
	new_td = createtdnode(lapartition.getElementsByTagName("description")[0].firstChild.nodeValue);
	old_td.parentNode.replaceChild(new_td, old_td);
	// Fichier
	hidden_val = document.getElementById("verif_session").value;
	old_td = tablehtml.getElementsByTagName("tr")[6].getElementsByTagName("td")[1];
	var newa = document.createElement("a");
	var newtextnode;
	var nomfichier = lapartition.getElementsByTagName("fichier")[0].firstChild.nodeValue;
	if (hidden_val.localeCompare("yes") == 0) {
		if (nomfichier.localeCompare("Aucun") == 0) {
			new_td = document.createElement("td");
			new_td.class = "disabledlink";
			new_td.appendChild(document.createTextNode(nomfichier));
			old_td.parentNode.replaceChild(new_td, old_td);
		}
		else {
			newa.href = "partitions/" + nomfichier;
			newa.appendChild(document.createTextNode(nomfichier));
			new_td = document.createElement("td");
			new_td.appendChild(newa);
			old_td.parentNode.replaceChild(new_td, old_td);
		}
	}
	else {
		newa.href = "login.php";
		newa.appendChild(document.createTextNode("Vous devez être connecté!"));
		new_td = document.createElement("td");
		new_td.appendChild(newa);
		old_td.parentNode.replaceChild(new_td, old_td);
	}
}

function XHRStateIsReady(xhr) {
	var isReady = false;
	
	if (xhr.readyState == 4) {
		if (xhr.status == 200) {
			isReady = true;
		}
		else {
			alert("Erreur " + xhr.status + " (" + xhr.statusText + ")");
		}
	}
	
	return isReady;
}

function createtdnode(text) {
	var newTD = document.createElement("td");
	var newTDtext = document.createTextNode(text);
			
	newTD.appendChild(newTDtext);
	
	return newTD;
}

function delPartForUser(userid) {
	var choice;
	var choiceFound = false;
	var table = document.getElementsByTagName("table")[0];
	var confirmation = false;
	document.getElementById("errortxt").firstChild.nodeValue = " ";
	for (i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].getElementsByTagName("input")[0].checked) {
			choice = table.getElementsByTagName("tr")[i].getElementsByTagName("input")[0].value;
			choiceFound = true;
			break;
		}
	}
	if (choiceFound) {
		confirmation = confirm("Êtes-vous sur de vouloir effacer cette partition?");
		if (confirmation)
			delDbPart(choice, userid);
		else
			return;
	}
	else {
		document.getElementById("errortxt").firstChild.nodeValue = "Indiquez tout d'abord la partition à supprimer en cochant la case sur sa gauche";
	}
}

function delDbPart(choice, userid) {
	var xhr = GetXHR();
	
	xhr.onreadystatechange = function() {
		if (XHRStateIsReady(xhr)) {
			delDbPartReply(xhr.responseXML, choice);
		}
	}
	xhr.open("GET", "data/part_del.php?userid=" + userid + "&partid=" + choice, true);
	xhr.send(null);
}

function delDbPartReply(xmldoc, choice) {
	var table;
	// Trouver et éliminer le TR cible
	table = document.getElementsByTagName("table")[0];
	for (var i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].firstChild.value == choice) {
			table.deleteRow(i);
			break;
		}
	}
}

function delFavForUser(userid) {
	var choice;
	var choiceFound = false;
	var table = document.getElementsByTagName("table")[0];
	var confirmation = false;
	document.getElementById("errortxt").firstChild.nodeValue = " ";
	for (i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].firstChild.getElementsByTagName("input")[1].checked) {
			choice = table.getElementsByTagName("tr")[i].firstChild.getElementsByTagName("input")[0].value;
			choiceFound = true;
			break;
		}
	}
	if (choiceFound) {
		confirmation = confirm("Êtes-vous sur de vouloir effacer ce favori de votre liste?");
		if (confirmation)
			delDbFav(choice, userid);
		else
			return;
	}
	else {
		document.getElementById("errortxt").firstChild.nodeValue = "Indiquez tout d'abord la partition à supprimer en cochant la case sur sa gauche";
	}
}

function delDbFav(choice, userid) {
	var xhr = GetXHR();
	
	xhr.onreadystatechange = function() {
		if (XHRStateIsReady(xhr)) {
			delDbFavReply(xhr.responseXML, choice);
		}
	}
	xhr.open("GET", "data/fav_del.php?userid=" + userid + "&favid=" + choice, true);
	xhr.send(null);
}

function delDbFavReply(xmldoc, choice) {
	var table;
	// Trouver et éliminer le TR cible
	table = document.getElementsByTagName("table")[0];
	for (var i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].firstChild.value == choice) {
			table.deleteRow(i);
			break;
		}
	}
}

function addFavForUser(userid) {
	var choice;
	var choiceFound = false;
	var table = document.getElementsByTagName("table")[0];
	document.getElementById("errortxt").firstChild.nodeValue = " ";
	for (i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].firstChild.getElementsByTagName("input")[1].checked) {
			choice = table.getElementsByTagName("tr")[i].firstChild.getElementsByTagName("input")[0].value;
			choiceFound = true;
			break;
		}
	}
	if (choiceFound) {
		addDbFav(choice, userid);
	}
	else {
		document.getElementById("errortxt").firstChild.nodeValue = "Indiquez tout d'abord la partition à ajouter en cochant la case sur sa gauche";
	}
}

function addDbFav(choice, userid) {
	var xhr = GetXHR();
	
	xhr.onreadystatechange = function() {
		if (XHRStateIsReady(xhr)) {
			addDbFavReply(xhr.responseXML, choice);
		}
	}
	xhr.open("GET", "data/fav_add.php?userid=" + userid + "&favid=" + choice, true);
	xhr.send(null);
}

function addDbFavReply(xmldoc, choice) {
	var erreur = false;
	var table;
	if (xmldoc.getElementsByTagName("erreur").length > 0) {
		// Erreur: le favori existe déjà
		document.getElementById("errortxt").firstChild.nodeValue = xmldoc.getElementsByTagName("erreur")[0].firstChild.nodeValue;
	}
	else {
		// Trouver le TR cible
		table = document.getElementsByTagName("table")[0];
		for (var i = 1; i < table.getElementsByTagName("tr").length; i++) {
			if (table.getElementsByTagName("tr")[i].getElementsByTagName("td")[0].firstChild.value == choice) {
				table.getElementsByTagName("tr")[i].setAttribute("class", "fonce");
				break;
			}
		}
	}
}

function validChoice(objet) {
	var choice;
	var choiceFound = false;
	var table = document.getElementsByTagName("table")[0];
	var action = 0;
	var actionString = "";
	if (objet.value.localeCompare("Ajouter") == 0) {
		document.location.href = "edition_part.php?action=Ajouter";
		return;
	}
	else if (objet.value.localeCompare("Modifier") == 0) {
		action = 1; // Modifier
		actionString = "à modifier";
	}
	document.getElementById("errortxt").firstChild.nodeValue = " ";
	for (i = 1; i < table.getElementsByTagName("tr").length; i++) {
		if (table.getElementsByTagName("tr")[i].getElementsByTagName("input")[0].checked) {
			choice = table.getElementsByTagName("tr")[i].getElementsByTagName("input")[0].value;
			choiceFound = true;
			break;
		}
	}
	if (choiceFound) {
		if (action == 1) // Modifier
			document.location.href = "edition_part.php?action=Modifier&part_choice=" + choice;
	}
	else {
		document.getElementById("errortxt").firstChild.nodeValue = "Indiquez tout d'abord la partition " + actionString + " en cochant la case sur sa gauche";
		return false;
	}
}

function newFileAsk(fichierActuel, partChoice, lastAction) {
	var ok = confirm("Attention: les données non sauvegardées du formulaire seront perdues. Continuer?");
	if (ok) {
		document.location.href = "upload.php?fichierActuel=" + fichierActuel + "&partChoice=" + partChoice + "&lastAction=" + lastAction;
	}
	else
		return;
}

function goToParts(partChoice, lastAction) {
	if (lastAction.localeCompare("Ajouter") == 0) {
		// Nouvelle partition créée mais l'usager annule l'envoi de fichier.
		// Il faut alors enlever le nombre généré au hasard, placé dans projet_partitions -> FichierXml
		// et le remplacer par "Aucun" tant que l'usager n'enverra pas de fichier.
		document.location.href = "index.php?type=my&partid=" + partChoice + "&changeFile=" + partChoice;
	}
	else
		document.location.href = "edition_part.php?action=" + lastAction + "&part_choice=" + partChoice;
}

function changerFichier(partChoice) {
	var xhr = GetXHR();
	
	xhr.open("GET", "data/changer_fichier.php?id=" + partChoice, true);
	xhr.send(null);
}

function verif_edit_part() {
	var errorfound = false;
	var titre = document.getElementById("txtTitre");
	var artiste = document.getElementById("txtArtiste");
	var desc = document.getElementById("txtDesc");
	var longueurChaine = 0;
	document.getElementById("errTitre").firstChild.nodeValue = "";
	document.getElementById("errArtiste").firstChild.nodeValue = "";
	// Check si vide
	if (titre.value == "") {
		document.getElementById("errTitre").firstChild.nodeValue = "Le titre ne peut pas être vide.";
		errorfound = true;
	}
	else {
		longueurChaine = titre.value.length;
		titre.value = cleanString(titre.value);
		if (longueurChaine != titre.value.length) {
			document.getElementById("errTitre").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	if (artiste.value == "") {
		document.getElementById("errArtiste").firstChild.nodeValue = "L'artiste ne peut pas être vide.";
		errorfound = true;
	}
	else {
		longueurChaine = artiste.value.length;
		artiste.value = cleanString(artiste.value);
		if (longueurChaine != artiste.value.length) {
			document.getElementById("errArtiste").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	// Si la description n'existe pas ou est vide, placer un espace dans le champ pour que le "child" existe. Évite une erreur dans l'affichage des infos.
	if (desc.value == null) {
		desc.value = " ";
	}
	else if (desc.value == "") {
		desc.value = " ";
	}
	else {
		longueurChaine = desc.value.length;
		desc.value = cleanString(desc.value);
		if (longueurChaine != desc.value.length) {
			document.getElementById("errDesc").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	if (errorfound)
		return false;
	else
		return true;
}

function verif_login() {
	var loginname = document.getElementById("txtNom");
	var loginpass = document.getElementById("txtMotPasse");
	var longueurChaine = 0;
	var errorfound = false;
	document.getElementById("errloginname").firstChild.nodeValue = "";
	document.getElementById("errpass").firstChild.nodeValue = "";
	if (loginname.value == "") {
		document.getElementById("errloginname").firstChild.nodeValue = "Le nom d'usager ne peut pas être vide.";
		errorfound = true;
	}
	else {
		longueurChaine = loginname.value.length;
		loginname.value = cleanString(loginname.value);
		if (longueurChaine != loginname.value.length) {
			document.getElementById("errloginname").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	if (loginpass.value == "") {
			document.getElementById("errpass").firstChild.nodeValue = "Le mot de passe ne peut pas être vide.";
			errorfound = true;		
	}
	else {
		longueurChaine = loginpass.value.length;
		loginpass.value = cleanString(loginpass.value);
		if (longueurChaine != loginpass.value.length) {
			loginpass.value = "";
			document.getElementById("errpass").firstChild.nodeValue = "Des caractères interdits ont été détectés. Recommencez svp.";
			errorfound = true;
		}
	}
	if (errorfound)
		return false;
	else
		return true;
}

function verif_edit_user(action) {
	// action: 1 = Ajouter, 2 = Éditer
	var nom = document.getElementById("txtNom");
	var loginname = document.getElementById("txtNomUtilisateur");
	var passwd = document.getElementById("txtMotDePasse");
	var passwdconf = document.getElementById("txtConfirmation");
	var longueurChaine = 0;
	var errorfound = false;
	document.getElementById("errnom").firstChild.nodeValue = "";
	document.getElementById("errloginname").firstChild.nodeValue = "";
	document.getElementById("errpass").firstChild.nodeValue = "";
	if (nom.value == "") {
		document.getElementById("errnom").firstChild.nodeValue = "Le nom ne peut pas être vide.";
		errorfound = true;
	}
	else {
		longueurChaine = nom.value.length;
		nom.value = cleanString(nom.value);
		if (longueurChaine != nom.value.length) {
			document.getElementById("errnom").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	if (loginname.value == "") {
		document.getElementById("errloginname").firstChild.nodeValue = "Le nom d'usager ne peut pas être vide.";
		errorfound = true;
	}
	else {
		longueurChaine = loginname.value.length;
		loginname.value = cleanString(loginname.value);
		if (longueurChaine != loginname.value.length) {
			document.getElementById("errloginname").firstChild.nodeValue = "Des caractères interdits ont été enlevés. Revoyez l'entrée svp.";
			errorfound = true;
		}
	}
	if (passwd.value != passwdconf.value) {
		document.getElementById("errpass").firstChild.nodeValue = "Les mots de passe doivent correspondre. Recommencez svp.";
		document.getElementById("txtMotDePasse").value = "";
		document.getElementById("txtConfirmation").value = "";
		errorfound = true;
	}
	else if (passwd.value != "") {
		longueurChaine = passwd.value.length;
		passwd.value = cleanString(passwd.value);
		if (longueurChaine != passwd.value.length) {
			document.getElementById("errpass").firstChild.nodeValue = "Des caractères interdits ont été détectés. Recommencez svp.";
			document.getElementById("txtMotDePasse").value = "";
			document.getElementById("txtConfirmation").value = "";
			errorfound = true;
		}
	}
	else {
		if (action == 1) {
			// Action: ajouter: mot de passe obligatoire
			document.getElementById("errpass").firstChild.nodeValue = "Le mot de passe ne peut pas être vide.";
			errorfound = true;
		}
	}
	
	if (errorfound)
		return false;
	else
		return true;
}

function cleanString(str) {
	return str.replace(/[`~!@#$%^&*±£¢¤¬¦²³¼½¾´­¯^()|+\=÷¿?;'",.<>\{\}\[\]\\\/]/gi, '');
}