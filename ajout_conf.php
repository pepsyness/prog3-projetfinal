<?php
include('includes/constantes2.php');
include('includes/functions.php');
if(session_id() == '') {
    session_start();
}
?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title>Création d'usager - confirmation</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>

		<div id="content">
			<span class="spanconfmsg">L'usager a été créé. Merci!</span>
			<br /><br />
			<input type="button" class="btn" name="retourindex" value="Accueil" onclick="location.href='index.php';" />&nbsp;&nbsp;
			<input type="button" class="btn" name="retourlogin" value="Se connecter" onclick="location.href='login.php';" />
			<br /><br />
		</div>
		<?php include("includes/footer.php"); ?>
	</body>
</html>