<?php
include('includes/constantes2.php');
if(session_id() == '') {
    session_start();
}
$action = "";
$modeAdd = false;
$modeMod = false;
$modeDel = false;
$part_choice = '';
if (!isset($_SESSION['userid']))
	Header("Location: index.php");
if (isset($_GET['part_choice']))
	$part_choice = $_GET['part_choice'];
if (isset($_GET["action"])) {
	$action = $_GET["action"];
	
	switch ($action) {
		case "Ajouter":
			$page_titre = "Ajouter une partition";
			$bouton_texte = "Ajouter";
			$modeAdd = true;
			break;
		
		case "Modifier":
			$page_titre = "Modifier une partition";
			$bouton_texte = "Enregistrer";
			$modeMod = true;
			break;
			
		case "Supprimer":
			$page_titre = "Supprimer une partition";
			$bouton_texte = "Supprimer";
			$modeDel = true;
			break;
	}
}
?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title><?php echo $page_titre ?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<?php include("includes/header.php"); ?>
		<?php include("includes/menu.php"); ?>

		<div id="content">
			<span class="title"><?php echo $page_titre; ?></span>
			<br /><br />
		
			<form method="post" action="traitement_partition.php" onsubmit="return verif_edit_part();">
				<?php
				if ($modeAdd) {
					echo '<input type="hidden" name="action" value="ajouter" />';
				}
				else if ($modeMod) {
					echo '<input type="hidden" name="part_id" value="' . $part_choice . '" />';
					echo '<input type="hidden" name="action" value="modifier" />';
				}
				else if ($modeDel) {
					echo '<input type="hidden" name="action" value="supprimer" />';
				}
				?>
				<table class="edition">
					<?php
						$titre = "";
						$genre = "";
						$artiste = "";
						$acces = "";
						$fichier = "";
						$desc = "";
						if ($modeMod || $modeDel) {
								$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
								mysql_select_db(DB_NAME, $db);
								$requete = 'SELECT * FROM projet_partitions WHERE UsagerID = ' . $_SESSION['userid'] .
												' AND ID = ' . $part_choice;
								$result = mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
								$rowreqA = mysql_fetch_assoc($result);
								$titre = $rowreqA['Titre'];
								$genre = $rowreqA['Genre'];
								$artiste = $rowreqA['Artiste'];
								$acces = $rowreqA['IsPublic'];
								$fichier = $rowreqA['FichierXml'];
								$desc = $rowreqA['Description'];
						}
						?>
					<tr>
						<td>Titre: </td>
						<td><input class="edition" type="text" id="txtTitre" name="txtTitre" size="30" maxlength="30" value="<?php echo $titre; ?>" /></td>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errTitre"> </span></td>
					</tr>
					<tr>
						<td>Genre: </td><td>
							<select name="selGenre">
								<?php
									$db = @mysql_connect(DB_HOST, DB_LOGIN, DB_PASS);
									mysql_select_db(DB_NAME, $db);
									$requete = 'SELECT * FROM projet_genres ORDER BY Nom';
									$result = @mysql_query($requete) or die("Erreur SQL !<br />" . $requete . "<br />" . mysql_error());
									while ($rowreqB = mysql_fetch_assoc($result)) {
										if (($modeMod || $modeDel) && strcmp($rowreqB['Nom'], $genre) == 0)
											echo '<option selected value="' . $rowreqB['Nom'] . '">' . $rowreqB['Nom'] . '</option>';
										else
											echo '<option value="' . $rowreqB['Nom'] . '">' . $rowreqB['Nom'] . '</option>';
									}
									mysql_close();
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Artiste : </td>
						<td><input class="edition" type="text" name="txtArtiste" size="30" maxlength="30" id="txtArtiste" value="<?php echo $artiste; ?>" /></td>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errArtiste"> </span></td>
					</tr>
					<tr>
						<td>Publique : </td>
						<?php
							if (($modeMod || $modeDel) && isset($rowreqA) && $rowreqA['IsPublic'] == 1)
								echo '<td><input class="edition" type="checkbox" name="chkPublique" checked /></td>';
							else
								echo '<td><input class="edition" type="checkbox" name="chkPublique" /></td>';
						?>
					</tr>
					<?php
						if (!$part_choice == null) {
							// Mode modification actif
							echo '<tr>';
							echo '<td>Fichier XML : </td><td colspan="2">' . $fichier . '&nbsp;&nbsp;<input type="button" name="newFile" value="Nouveau" onclick="newFileAsk(\'' .
										$fichier . '\', ' . $part_choice . ', \'' . $action . '\');" /></td>';
							echo '</tr>';
						}
					?>
					<tr>
						<td>Description : </td>
						<td><textarea rows="4" cols="32" name="txtareaDesc" maxlength="192" id="txtDesc"><?php echo $desc; ?></textarea></td>
					</tr>
					<tr class="trerror">
						<td colspan="3" style="text-align:center"><span id="errDesc"> </span></td>
					</tr>
				</table>

				<br />
				<input type="submit" class="btn" value="<?php echo $bouton_texte; ?>" />
				<input type="button" class="btn" value="Annuler" onclick="document.location.href = 'index.php?type=my'" />
				<br /><br />
			</form>
		</div>
		
		<?php include("includes/footer.php"); ?>
	</body>
</html>
